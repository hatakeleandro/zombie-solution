using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace ZumbieSolution.Migrations
{
    public partial class rerrecoes2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Recurso_RecursoId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Usuario_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Recurso_RecursoId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Usuario_ResponsavelId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Usuario_Funcao_FuncaoId", table: "Usuario");
            migrationBuilder.AddColumn<int>(
                name: "CategoriaId",
                table: "Recurso",
                nullable: false,
                defaultValue: 0);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Recurso_RecursoId",
                table: "Re_entrada",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Usuario_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Recurso_RecursoId",
                table: "Re_saida",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Usuario_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Recurso_Categoria_CategoriaId",
                table: "Recurso",
                column: "CategoriaId",
                principalTable: "Categoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Usuario_Funcao_FuncaoId",
                table: "Usuario",
                column: "FuncaoId",
                principalTable: "Funcao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Recurso_RecursoId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Usuario_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Recurso_RecursoId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Usuario_ResponsavelId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Recurso_Categoria_CategoriaId", table: "Recurso");
            migrationBuilder.DropForeignKey(name: "FK_Usuario_Funcao_FuncaoId", table: "Usuario");
            migrationBuilder.DropColumn(name: "CategoriaId", table: "Recurso");
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Recurso_RecursoId",
                table: "Re_entrada",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Usuario_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Recurso_RecursoId",
                table: "Re_saida",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Usuario_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Usuario_Funcao_FuncaoId",
                table: "Usuario",
                column: "FuncaoId",
                principalTable: "Funcao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
