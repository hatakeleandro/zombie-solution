using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace ZumbieSolution.Migrations
{
    public partial class criandoModelos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Pessoa_Funcao_FuncaoId", table: "Pessoa");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Pessoa_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Pessoa_ResponsavelId", table: "Re_saida");
            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_Funcao_FuncaoId",
                table: "Pessoa",
                column: "FuncaoId",
                principalTable: "Funcao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Pessoa_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Pessoa_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Pessoa_Funcao_FuncaoId", table: "Pessoa");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Pessoa_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Pessoa_ResponsavelId", table: "Re_saida");
            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_Funcao_FuncaoId",
                table: "Pessoa",
                column: "FuncaoId",
                principalTable: "Funcao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Pessoa_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Pessoa_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
