using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace ZumbieSolution.Migrations
{
    public partial class Correcoes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Recurso_RecursoId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Pessoa_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Recurso_RecursoId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Pessoa_ResponsavelId", table: "Re_saida");
            migrationBuilder.DropTable("Pessoa");
            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FuncaoId = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Sexo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Usuario_Funcao_FuncaoId",
                        column: x => x.FuncaoId,
                        principalTable: "Funcao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Recurso_RecursoId",
                table: "Re_entrada",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Usuario_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Recurso_RecursoId",
                table: "Re_saida",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Usuario_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Recurso_RecursoId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_entrada_Usuario_ResponsavelId", table: "Re_entrada");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Recurso_RecursoId", table: "Re_saida");
            migrationBuilder.DropForeignKey(name: "FK_Re_saida_Usuario_ResponsavelId", table: "Re_saida");
            migrationBuilder.DropTable("Usuario");
            migrationBuilder.CreateTable(
                name: "Pessoa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataNascimento = table.Column<DateTime>(nullable: false),
                    FuncaoId = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Sexo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pessoa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pessoa_Funcao_FuncaoId",
                        column: x => x.FuncaoId,
                        principalTable: "Funcao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Recurso_RecursoId",
                table: "Re_entrada",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_entrada_Pessoa_ResponsavelId",
                table: "Re_entrada",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Recurso_RecursoId",
                table: "Re_saida",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Re_saida_Pessoa_ResponsavelId",
                table: "Re_saida",
                column: "ResponsavelId",
                principalTable: "Pessoa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
