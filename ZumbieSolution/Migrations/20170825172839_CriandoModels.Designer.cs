using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using ZumbieSolution.DAO;

namespace ZumbieSolution.Migrations
{
    [DbContext(typeof(ZumbieContext))]
    [Migration("20170825172839_CriandoModels")]
    partial class CriandoModels
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ZumbieSolution.Models.Categoria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao");

                    b.Property<string>("Nome");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Funcao", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao");

                    b.Property<string>("Nome");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Pessoa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DataNascimento");

                    b.Property<int>("FuncaoId");

                    b.Property<string>("Nome");

                    b.Property<string>("Sexo");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Re_entrada", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Data");

                    b.Property<int>("Qtd");

                    b.Property<int>("ResponsavelId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Re_saida", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Data");

                    b.Property<int>("Qtd");

                    b.Property<int>("ResponsavelId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Recurso", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao");

                    b.Property<string>("Nome");

                    b.Property<int>("QtdTotal");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Pessoa", b =>
                {
                    b.HasOne("ZumbieSolution.Models.Funcao")
                        .WithMany()
                        .HasForeignKey("FuncaoId");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Re_entrada", b =>
                {
                    b.HasOne("ZumbieSolution.Models.Pessoa")
                        .WithMany()
                        .HasForeignKey("ResponsavelId");
                });

            modelBuilder.Entity("ZumbieSolution.Models.Re_saida", b =>
                {
                    b.HasOne("ZumbieSolution.Models.Pessoa")
                        .WithMany()
                        .HasForeignKey("ResponsavelId");
                });
        }
    }
}
