﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZumbieSolution.Models
{
    public class Recurso
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int QtdTotal { get; set; }
        public virtual Categoria Categoria { get; set; }
        public int CategoriaId { get; set; }
        public string Descricao { get; set; }

        
    }
}