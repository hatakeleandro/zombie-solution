﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZumbieSolution.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public virtual Funcao Funcao { get; set; }
        public int FuncaoId { get; set; }
    }
}