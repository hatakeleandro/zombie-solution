﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZumbieSolution.Models
{
    public abstract class Movimentacao
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int Qtd { get; set; }
        public virtual Recurso Recurso { get; set; }
        public int RecursoId { get; set; }
        public virtual Usuario Responsavel { get; set; }
        public int ResponsavelId { get; set; }

    }
}