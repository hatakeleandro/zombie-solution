﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZumbieSolution.Models
{
    public class Funcao
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}