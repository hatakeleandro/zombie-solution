﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class UsuarioController : Controller
    {
         public ActionResult Index()
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            ViewBag.Usuarios = usuarioDao.List();
            return View();
        }

        public ActionResult Novo(int id = 0)
        {
            Usuario usuario;
            if (id.Equals(null) || id.Equals(0))
            {
                usuario = new Usuario();
            }
            else
            {
                UsuarioDao dao = new UsuarioDao();
                usuario = dao.Find(id);
            }
            FuncaoDao funcaoDao = new FuncaoDao();

            ViewBag.Funcoes = funcaoDao.List();
            ViewBag.Usuario = usuario;
            return View();
        }

        public ActionResult Cadastro(Usuario usuario)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            if (usuario.Id.Equals(null) || usuario.Id.Equals(0))
            {
                usuarioDao.Insert(usuario);
            }
            else
            {
                usuarioDao.Update(usuario);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            UsuarioDao dao = new UsuarioDao();
            dao.DeleteById(id);
            return Json(id);
        }
    }
}