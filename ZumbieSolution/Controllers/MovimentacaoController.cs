﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class MovimentacaoController : Controller
    {
        // GET: Movimentacao
        public ActionResult Entrada(Re_entrada entrada)
        {
            MovimentacaoDao dao = new MovimentacaoDao();
            entrada.Data = DateTime.Now;
            dao.Entrada(entrada);
            return RedirectToAction("Index","Home");
        }

        public ActionResult saida(Re_saida saida)
        {
            MovimentacaoDao dao = new MovimentacaoDao();
            saida.Data = DateTime.Now;
            dao.Saida(saida);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Entradas()
        {
            MovimentacaoDao dao = new MovimentacaoDao();
            ViewBag.Entradas = dao.EntradaList();
            return View();
        }

        public ActionResult Saidas()
        {
            MovimentacaoDao dao = new MovimentacaoDao();
            ViewBag.Saidas = dao.SaidaList();
            return View();
        }
    }
}