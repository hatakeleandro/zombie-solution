﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class CategoriaController : Controller
    {
        // GET: Categoria
        public ActionResult Index()
        {
            CategoriaDao categoriaDao = new CategoriaDao();
            ViewBag.Categorias = categoriaDao.List();
            return View();
        }

        public ActionResult Novo(int id =0)
        {
            Categoria categoria;
            if (id.Equals(null) || id.Equals(0))
            {
                categoria = new Categoria();
            }
            else
            {
                CategoriaDao dao = new CategoriaDao();
                categoria = dao.Find(id);
            }
            ViewBag.Categoria = categoria;
            return View();
        }

        public ActionResult Cadastro(Categoria categoria)
        {
            CategoriaDao categoriaDao = new CategoriaDao();
            if (categoria.Id.Equals(null) || categoria.Id.Equals(0))
            {
                categoriaDao.Insert(categoria);
            }
            else
            {
                categoriaDao.Update(categoria);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            CategoriaDao dao = new CategoriaDao();
            dao.DeleteById(id);
            return Json(id);
        }

    }
}