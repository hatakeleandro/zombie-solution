﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class RecursoController : Controller
    {
        // GET: Recurso
        public ActionResult Index()
        {
            RecursoDao recursoDao = new RecursoDao();
            ViewBag.Recursos = recursoDao.List();
            return View();
        }

        public ActionResult Novo(int id = 0)
        {
            Recurso recurso;
            if (id.Equals(null) || id.Equals(0))
            {
                recurso = new Recurso();
            }
            else
            {
                RecursoDao dao = new RecursoDao();
                recurso = dao.Find(id);
            }
            CategoriaDao categoriaDao = new CategoriaDao();
            ViewBag.Categorias = categoriaDao.List();
            ViewBag.Recurso = recurso;
            return View();
        }

        public ActionResult Cadastro(Recurso recurso)
        {
            RecursoDao recursoDao = new RecursoDao();
            if (recurso.Id.Equals(null) || recurso.Id.Equals(0))
            {
                recursoDao.Insert(recurso);
            }
            else
            {
                recursoDao.Update(recurso);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            RecursoDao dao = new RecursoDao();
            dao.DeleteById(id);
            return Json(id);
        }
    }
}