﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            string nome = Request.QueryString["nome"];
            string nomeCategoria = Request.QueryString["nomeCategoria"];

            RecursoDao recursoDao = new RecursoDao();
            ViewBag.Recursos = recursoDao.BuscaPorNomeCategoriat(nome,nomeCategoria);



            CategoriaDao categoriaDao = new CategoriaDao();
            ViewBag.Categorias = categoriaDao.List();
            return View();
        }

        public ActionResult Novo()
        {
            return View();
        }
    }
}