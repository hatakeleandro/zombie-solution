﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;

namespace ZumbieSolution.Controllers
{
    public class ModalController : Controller
    {
        // GET: Modal
        public ActionResult Entrada()
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            ViewBag.Usuarios = usuarioDao.List();
            return View();
        }
        public ActionResult Saida()
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            ViewBag.Usuarios = usuarioDao.List();
            return View();
        }
    }
}