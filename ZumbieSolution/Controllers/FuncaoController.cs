﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZumbieSolution.DAO;
using ZumbieSolution.Models;

namespace ZumbieSolution.Controllers
{
    public class FuncaoController : Controller
    {
        // GET: Funcao
        public ActionResult Index()
        {
            FuncaoDao funcaoDao = new FuncaoDao();
            ViewBag.Funcoes = funcaoDao.List();
            return View();
        }

        public ActionResult Novo(int id = 0)
        {
            Funcao funcao;
            if (id.Equals(null) || id.Equals(0))
            {
                funcao = new Funcao();
            }
            else
            {
                FuncaoDao dao = new FuncaoDao();
                funcao = dao.Find(id);
            }
            ViewBag.Funcao = funcao;
            return View();
        }

        public ActionResult Cadastro(Funcao funcao)
        {
            FuncaoDao funcaoDao = new FuncaoDao();
            if (funcao.Id.Equals(null) || funcao.Id.Equals(0))
            {
                funcaoDao.Insert(funcao);
            }
            else
            {
                funcaoDao.Update(funcao);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            FuncaoDao dao = new FuncaoDao();
            dao.DeleteById(id);
            return Json(id);
        }
    }
}