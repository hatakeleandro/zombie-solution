﻿
    $(document).ready(function () {
        $(".entrada").click(function () {
            var id = $(this).attr("recursoId");
            var nome = $(this).attr("recursoNome");
            var modal = $("#entrada");
            modal.find(".id").val(id);
            modal.find(".nome").html(nome);

            modal.modal('show') 
        });

        $(".saida").click(function () {
            var id = $(this).attr("recursoId");
            var nome = $(this).attr("recursoNome");
            var modal = $("#saida");
            modal.find(".id").val(id);
            modal.find(".nome").html(nome);

            modal.modal('show')
        });

    });

