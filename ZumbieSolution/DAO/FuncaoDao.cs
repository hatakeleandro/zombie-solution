﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class FuncaoDao
    {
        private ZumbieContext ctx;
        public FuncaoDao()
        {
            ctx = new ZumbieContext();
        }
        public void Insert(Funcao funcao)
        {
            this.ctx.Funcoes.Add(funcao);
            this.ctx.SaveChanges();
        }
        public void Update(Funcao funcao)
        {
            this.ctx.Funcoes.Update(funcao);
            this.ctx.SaveChanges();
        }
        public void Delete(Funcao funcao)
        {
            this.ctx.Funcoes.Remove(funcao);
            this.ctx.SaveChanges();
        }
        public IList<Funcao> List()
        {
            return this.ctx.Funcoes.ToList();
        }

        public Funcao Find(int id)
        {
            return this.ctx.Funcoes.FirstOrDefault(c => c.Id == id);
        }

        public void DeleteById(int id)
        {
            this.ctx.Funcoes.Remove(this.Find(id));
            this.ctx.SaveChanges();
        }
    }
}