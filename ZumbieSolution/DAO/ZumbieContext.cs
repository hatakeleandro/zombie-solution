﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class ZumbieContext:DbContext
    {
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Funcao> Funcoes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Re_entrada> Re_entradas { get; set; }
        public DbSet<Re_saida> Re_saidas { get; set; }
        public DbSet<Recurso> Recursos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            string conexaoString = ConfigurationManager.ConnectionStrings["Db_zumbieConnectionString"].ConnectionString; 
            optionsBuilder.UseSqlServer(conexaoString);

            base.OnConfiguring(optionsBuilder);
        }
    }
}