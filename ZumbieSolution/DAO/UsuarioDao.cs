﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class UsuarioDao
    {
        private ZumbieContext ctx;
        public UsuarioDao()
        {
            ctx = new ZumbieContext();
        }
        public void Insert(Usuario usuario)
        {
            this.ctx.Usuarios.Add(usuario);
            this.ctx.SaveChanges();
        }
        public void Update(Usuario usuario)
        {
            this.ctx.Usuarios.Update(usuario);
            this.ctx.SaveChanges();
        }
        public void Delete(Usuario usuario)
        {
            this.ctx.Usuarios.Remove(usuario);
            this.ctx.SaveChanges();
        }
        public IList<Usuario> List()
        {
            return ctx.Usuarios.Include(p => p.Funcao).ToList();
        }

        public Usuario Find(int id)
        {
            return this.ctx.Usuarios.FirstOrDefault(c => c.Id == id);
        }

        public void DeleteById(int id)
        {
            this.ctx.Usuarios.Remove(this.Find(id));
            this.ctx.SaveChanges();
        }
    }
}