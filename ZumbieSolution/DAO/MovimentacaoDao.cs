﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class MovimentacaoDao
    {
        private ZumbieContext ctx;
        public MovimentacaoDao()
        {
            ctx = new ZumbieContext();
        }
        public bool Entrada(Re_entrada entrada)
        {
            if (entrada.Qtd > 0)
            {
                RecursoDao recursoDao = new RecursoDao();

                Recurso recurso = recursoDao.Find(entrada.RecursoId);
                recurso.QtdTotal += entrada.Qtd;
                
                entrada.Recurso = recurso;
                this.ctx.Re_entradas.Add(entrada);

                this.ctx.SaveChanges();
                recursoDao.Update(recurso);
                return true;
            }
            return false;
        }
        public bool Saida(Re_saida saida)
        {
            if (saida.Qtd > 0)
            {
                RecursoDao recursoDao = new RecursoDao();

                Recurso recurso = recursoDao.Find(saida.RecursoId);
                recurso.QtdTotal -= saida.Qtd;

                saida.Recurso = recurso;
                this.ctx.Re_saidas.Add(saida);

                this.ctx.SaveChanges();
                recursoDao.Update(recurso);
                return true;
            }
            return false;
        }

        public IList<Re_saida> SaidaList()
        {
            return this.ctx.Re_saidas.Include(r => r.Responsavel).ThenInclude(r => r.Funcao).Include(r => r.Recurso).ToList();
        }

        public IList<Re_entrada> EntradaList()
        {
            return ctx.Re_entradas.Include(r => r.Responsavel).Include(r => r.Recurso).ToList();
        }

    }
}