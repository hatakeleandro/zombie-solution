﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class RecursoDao
    {
        private ZumbieContext ctx;
        public RecursoDao()
        {
            ctx = new ZumbieContext();
        }
        public void Insert(Recurso recurso)
        {
            this.ctx.Recursos.Add(recurso);
            this.ctx.SaveChanges();
        }
        public void Update(Recurso recurso)
        {
            this.ctx.Recursos.Update(recurso);
            this.ctx.SaveChanges();
        }
        public void Delete(Recurso recurso)
        {
            this.ctx.Recursos.Remove(recurso);
            this.ctx.SaveChanges();
        }
        public IList<Recurso> List()
        {
            return ctx.Recursos.Include(r => r.Categoria).ToList();
        }

        public Recurso Find(int id)
        {
            return this.ctx.Recursos.FirstOrDefault(c => c.Id == id);
        }

        public void DeleteById(int id)
        {
            this.ctx.Recursos.Remove(this.Find(id));
            this.ctx.SaveChanges();
        }

        public IList<Recurso> BuscaPorNomeCategoriat(string nome, string categoria)
        {
            var busca = from r in ctx.Recursos.Include(r => r.Categoria) select r;

            if (!String.IsNullOrEmpty(nome))
            {
                busca = busca.Where(r => r.Nome.Contains(nome));
            }
            if (!String.IsNullOrEmpty(categoria))
            {
                busca = busca.Where(r => r.Categoria.Nome == categoria);
            }

            return busca.ToList();
        }
    }
}