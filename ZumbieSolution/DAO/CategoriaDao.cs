﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZumbieSolution.Models;

namespace ZumbieSolution.DAO
{
    public class CategoriaDao
    {
        private ZumbieContext ctx;
        public CategoriaDao()
        {
            ctx = new ZumbieContext();
        }
        public void Insert(Categoria categoria)
        {
            this.ctx.Categorias.Add(categoria);
            this.ctx.SaveChanges();
        }
        public void Update(Categoria categoria)
        {
            this.ctx.Categorias.Update(categoria);
            this.ctx.SaveChanges();
        }
        public void Delete(Categoria categoria)
        {
            this.ctx.Categorias.Remove(categoria);
            this.ctx.SaveChanges();
        }
        public IList<Categoria> List()
        {
            return this.ctx.Categorias.ToList();
        }

        public Categoria Find(int id)
        {
            return this.ctx.Categorias.FirstOrDefault(c => c.Id == id);
        }

        public void DeleteById(int id)
        {
            this.ctx.Categorias.Remove(this.Find(id));
            this.ctx.SaveChanges();
        }
    }
}